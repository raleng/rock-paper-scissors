module Game exposing (Game, create, gameDecoder, gameEncoder)

import Hashids
import Json.Decode exposing (Decoder, field)
import Json.Encode
import Time


type alias Game =
    { id : String }


gameDecoder : Decoder Game
gameDecoder =
    Json.Decode.map Game
        (field "id" Json.Decode.string)


gameEncoder : Game -> Json.Encode.Value
gameEncoder state =
    Json.Encode.object
        [ ( "id", Json.Encode.string state.id ) ]


create : Time.Posix -> Game
create time =
    let
        hashidsContext =
            Hashids.hashidsMinimum "rps" 6
    in
    { id = Hashids.encode hashidsContext <| Time.posixToMillis time }


isValidGame : String -> Bool
isValidGame id =
    True
