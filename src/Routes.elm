module Routes exposing (Route(..), parseUrl)

import Url exposing (Url)
import Url.Parser exposing ((</>), Parser, map, oneOf, parse, s, string, top)


type Route
    = Home
    | Game String
    | Unknown


parseRoute : Parser (Route -> a) a
parseRoute =
    oneOf
        [ map Home top
        , map Game (s "game" </> string)
        ]


parseUrl : Url -> Route
parseUrl url =
    Maybe.withDefault Unknown <| parse parseRoute url
