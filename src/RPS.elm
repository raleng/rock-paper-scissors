module RPS exposing (RPS, Winner)


type RPS
    = Rock
    | Paper
    | Scissors


type Winner
    = Tie
    | Winner RPS


determineWinner : RPS -> RPS -> Winner
determineWinner rps1 rps2 =
    case ( rps1, rps2 ) of
        ( Rock, Rock ) ->
            Tie

        ( Paper, Paper ) ->
            Tie

        ( Scissors, Scissors ) ->
            Tie

        ( Rock, Scissors ) ->
            Winner Rock

        ( Scissors, Rock ) ->
            Winner Rock

        ( Paper, Rock ) ->
            Winner Paper

        ( Rock, Paper ) ->
            Winner Paper

        ( Scissors, Paper ) ->
            Winner Scissors

        ( Paper, Scissors ) ->
            Winner Scissors
