module Main exposing (..)

-- ROCK-PAPER-SCISSORS

import Browser exposing (Document, UrlRequest(..))
import Browser.Navigation as Nav
import Element exposing (..)
import Element.Input as Input
import Game
import Html.Attributes exposing (id)
import Ports exposing (sendInfoOutside)
import Routes exposing (Route(..))
import Task
import Time
import Url



-- MAIN


main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }



-- MODEL
-- I can have picked an option,
-- Opponent can have picked an option


type alias Model =
    { key : Nav.Key
    , route : Route
    , gameId : Maybe String
    }


type RPS
    = Rock
    | Paper
    | Scissors


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( { key = key
      , gameId = Nothing
      , route = Routes.parseUrl url
      }
    , Cmd.none
    )



-- UPDATE


type Msg
    = LinkClicked UrlRequest
    | UrlChanged Url.Url
    | CreateGameClicked
    | CreateGame Time.Posix


update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of
        LinkClicked urlRequest ->
            case urlRequest of
                Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                External url ->
                    ( model, Nav.load url )

        UrlChanged url ->
            let
                route =
                    Routes.parseUrl url

                cmd =
                    case route of
                        Game id ->
                            Ports.sendInfoOutside <| Ports.JoinGame id

                        _ ->
                            Cmd.none
            in
            ( { model | route = route }, cmd )

        CreateGameClicked ->
            ( model, Task.perform CreateGame Time.now )

        CreateGame time ->
            let
                game =
                    Game.create time

                cmd =
                    Cmd.batch
                        [ Nav.pushUrl model.key ("game/" ++ game.id)
                        , Ports.sendInfoOutside <| Ports.CreateGame game
                        ]
            in
            ( model, cmd )



-- VIEW


view : Model -> Document Msg
view model =
    let
        body =
            case model.route of
                Home ->
                    viewHome model

                Game id ->
                    viewGame id

                Unknown ->
                    view404
    in
    { title = ""
    , body = [ Element.layout [] body ]
    }


viewHome : Model -> Element Msg
viewHome _ =
    column []
        [ Input.button [] { label = text "create game", onPress = Just CreateGameClicked }
        ]


{-| TODO: This view needs a check that the provided id is actually a valid game, and not a random string the user entered as a url
-}
viewGame : String -> Element Msg
viewGame id =
    column []
        [ el [] <| text id
        , link [] { label = text "home", url = "/" }
        ]


view404 : Element Msg
view404 =
    column []
        [ link [] { label = text "Bad game link, go back", url = "/" }
        ]
