port module Ports exposing (InfoForOutside(..), getInfoFromOutside, sendInfoOutside)

{-| TODO: users need to be connected ot the channel when they join a channel, not only when it is created

Complete the ports following Murphy Randles suggestions:

<https://github.com/mrmurphy/a-very-im-port-ant-topic/blob/master/example/src/OutsideInfo.elm>

-}

import Game exposing (Game)
import Json.Decode exposing (decodeValue)
import Json.Encode


type InfoForOutside
    = CreateGame Game
    | JoinGame String


type InfoForElm
    = GameStateChanged Game


type alias OutgoingData =
    { tag : String, data : Json.Encode.Value }


port infoForOutside : OutgoingData -> Cmd msg


port infoForElm : (OutgoingData -> msg) -> Sub msg


sendInfoOutside : InfoForOutside -> Cmd msg
sendInfoOutside info =
    case info of
        CreateGame game ->
            infoForOutside { tag = "CreateGame", data = Game.gameEncoder game }

        JoinGame id ->
            Cmd.none


getInfoFromOutside : (InfoForElm -> msg) -> (String -> msg) -> Sub msg
getInfoFromOutside tagger onError =
    infoForElm
        (\outsideInfo ->
            case outsideInfo.tag of
                "GameStateChanged" ->
                    case decodeValue Game.gameDecoder outsideInfo.data of
                        Ok game ->
                            tagger <| GameStateChanged game

                        Err e ->
                            onError <| Json.Decode.errorToString e

                _ ->
                    onError <| "Unexpected info from outside: " ++ Debug.toString outsideInfo
        )
