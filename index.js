import { Elm } from "./src/Main.elm";
const { createClient } = require('@supabase/supabase-js')

const supabase = createClient(
    process.env.SUPABASE_URL,
    process.env.SUPABASE_KEY
)


var app = Elm.Main.init({ node: document.getElementById("root") });

app.ports.createGame.subscribe((gameId) => {
    const channel = supabase.channel(gameId)

    function getRandomUser() {
      const users = ['Alice', 'Bob', 'Mallory', 'Inian']
      return users[Math.floor(Math.random() * users.length)]
    }

    function getUserId(count) {
        console.log('called with ', count)
        return 'user' + count
    }

    channel
      .on('presence', { event: 'sync' }, () => {
        console.log('currently online users', channel.presenceState())
      })
      .on('presence', { event: 'join' }, ({ newUser }) => {
        console.log('a new user has joined', newUser)
      })
      .on('presence', { event: 'leave' }, ({ leftUser }) =>
        console.log('a user has left', leftUser)
      )
      .subscribe(async (status) => {
        if (status === 'SUBSCRIBED') {
          let numberOfUsers = Object.keys(channel.presenceState()).length
          console.log('length first', numberOfUsers)
          const status = await channel.track({ user_name: getUserId(numberOfUsers) })
          console.log('length', Object.keys(channel.presenceState()).length)
          console.log('status', status)
        }
      })
});
